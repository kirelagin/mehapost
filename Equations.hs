{-# LANGUAGE GADTs, FlexibleInstances, MultiParamTypeClasses #-}

module Equations where


import Data.List
import qualified Data.Map.Lazy as M
import Data.Monoid
import qualified Data.Set as S
import qualified Numeric.LinearAlgebra as L

import Variable


data Polynomial t where
    Poly :: Num t => M.Map (Variable t) t -> t -> Polynomial t

instance Show t => Show (Polynomial t) where
    show (Poly m f) = ( foldr (\a b -> a ++ " + " ++ b) (show f)
                      . map (\(v,i) -> show i ++ "*" ++ show v)
                      . M.toList
                      ) m


class Num t => Polylike s t where
    asPoly :: s t -> Polynomial t

instance Num t => Polylike Polynomial t where
    asPoly = id

instance Num t => Polylike Variable t where
    asPoly v = flip Poly 0 . M.fromList $ [(v, 1)]


infixl 6 #+#, #-#, #+., .+#, #-., .-#
(#+#) :: (Num t, Polylike a t, Polylike b t) => a t -> b t -> Polynomial t
a #+# b =
    let (Poly m1 v1) = asPoly a
        (Poly m2 v2) = asPoly b
    in Poly (M.unionWith (+) m1 m2) (v1 + v2)
(#-#) :: (Num t, Polylike a t, Polylike b t) => a t -> b t -> Polynomial t
a #-# b = a #+# negatePoly (asPoly b)

(#+.) :: (Num t, Polylike a t) => a t -> t -> Polynomial t
a #+. b =
    let (Poly m1 v1) = asPoly a
    in Poly m1 (v1 + b)
(#-.) :: (Num t, Polylike a t) => a t -> t -> Polynomial t
a #-. b = a #+. (-b)

(.+#) :: (Num t, Polylike a t) => t -> a t -> Polynomial t
(.+#) = flip (#+.)
(.-#) :: (Num t, Polylike a t) => t -> a t -> Polynomial t
(.-#) = flip (#-.)


infix 4 #=#, #=., .=#
(#=#) :: (Num t, Polylike a t, Polylike b t) => a t -> b t -> Polynomial t
(#=#) = (#-#)
(#=.) :: (Num t, Polylike a t) => a t -> t -> Polynomial t
(#=.) = (#-.)
(.=#) :: (Num t, Polylike a t) => t -> a t -> Polynomial t
(.=#) = (.-#)

infixl 7 #*., .*#, #/.
(#*.) :: (Num t, Polylike a t) => a t -> t -> Polynomial t
a #*. b =
    let (Poly m1 v1) = asPoly a
    in Poly (M.map (*b) m1) (v1*b)
(.*#) :: (Num t, Polylike a t) => t -> a t -> Polynomial t
(.*#) = flip (#*.)

(#/.) :: (Fractional t, Polylike a t) => a t -> t -> Polynomial t
a #/. b = a #*. (1/b)


negatePoly :: Num t => Polynomial t -> Polynomial t
negatePoly (Poly m v) = Poly (M.map negate m) (-v)



instance Num t => Monoid (Polynomial t) where
    mempty = Poly M.empty 0
    mappend = (#+#)


type Equation t = Polynomial t

data EquationsSystem t = Eqs [Equation t] deriving (Show)

instance Monoid (EquationsSystem t) where
    mempty = Eqs []
    mappend (Eqs ps1) (Eqs ps2) = Eqs (ps1 ++ ps2)

size :: EquationsSystem t -> Int
size (Eqs l) = genericLength l

addEq :: EquationsSystem t -> Equation t -> EquationsSystem t
addEq (Eqs ps) p = Eqs (p:ps)

fromEqs :: [Equation t] -> EquationsSystem t
fromEqs = foldl addEq mempty

toMatrix :: L.Field t => EquationsSystem t -> (L.Matrix t, L.Vector t, [Variable t])
toMatrix (Eqs ps) =
    let vars = (S.toList . S.unions . map (\(Poly m _) -> M.keysSet m)) ps
        rows = map (\(Poly m _) -> map (\v -> M.findWithDefault 0 v m) vars) ps
        matrix = L.fromLists rows
        vect = L.fromList $ map (\(Poly _ v) -> -v) ps
    in
        (matrix, vect, vars)

solve :: L.Field t => EquationsSystem t -> Context t
solve s =
    let
        (m, vect, vars) = toMatrix s
        ans = L.linearSolve m (L.fromColumns [vect])
    in
        (zip vars . L.toList . L.flatten) ans

rank :: L.Field t => EquationsSystem t -> Int
rank (Eqs []) = 0
rank s        = let (m, _, _) = toMatrix s in L.rank m

enoughEquations :: L.Field t => EquationsSystem t -> Bool
enoughEquations s = let (m, _, _) = toMatrix s in L.cols m == L.rows m
