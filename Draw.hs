module Draw where

import Graphics.Rendering.Cairo hiding (Path)

import Draw.Style.Colour
import Util


type Point = (Double, Double)


data Path = OpenPath (Render ()) | ClosedPath (Render ())
renderPath :: Path -> Render ()
renderPath (OpenPath r) = r
renderPath (ClosedPath r) = r

type PaintAction = Render ()

data Brush = Brush {brushfc :: Colour
                   ,brushsc :: Colour
                   ,brushsw :: Double
                   }

strokePath :: Brush -> Path -> PaintAction
strokePath b p = do
    newPath
    renderPath p
    uncurry4 setSourceRGBA (brushsc b)
    setLineWidth (brushsw b)
    identityMatrix
    strokePreserve

paintPathWithBrush :: Brush -> Path -> PaintAction
paintPathWithBrush b p@(OpenPath _) = do
    save
    strokePath b p
    restore
paintPathWithBrush b p@(ClosedPath _) = do
    save
    strokePath b p
    uncurry4 setSourceRGBA (brushfc b)
    fill
    restore
