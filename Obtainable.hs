{-# LANGUAGE FlexibleContexts, FlexibleInstances, FunctionalDependencies, UndecidableInstances #-}

module Obtainable where


class Obtainable t c a | a t -> c where
    obtain :: a -> c -> t

instance Obtainable t c (c -> t) where
    obtain f c = f c

instance Obtainable t c t where
    obtain v _ = v


instance (Num t, Obtainable t c (c -> t)) => Num (c -> t) where
    (+) a b = \c -> (+) (obtain a c) (obtain b c)
    (-) a b = \c -> (-) (obtain a c) (obtain b c)
    (*) a b = \c -> (*) (obtain a c) (obtain b c)
    abs a = \c -> abs (obtain a c)
    signum a = \c -> signum (obtain a c)
    fromInteger n = \_ -> fromInteger n

instance (Fractional t, Obtainable t c (c -> t)) => Fractional (c -> t) where
    (/) a b = \c -> (/) (obtain a c) (obtain b c)
    fromRational n = \_ -> fromRational n
