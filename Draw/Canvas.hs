{-# LANGUAGE FlexibleContexts, FlexibleInstances #-}

module Draw.Canvas where


import Control.Monad.Trans.State
import Data.Monoid
import Graphics.Rendering.Cairo

import Draw
import Draw.Style
import Equations
import Obtainable
import Variable


type Canvas = ([Context Double -> Render ()], Brush, EquationsSystem Double, Integer)

emptyCanvas :: Canvas
emptyCanvas = ([], defaultBrush, mempty, 0)


type Picture = State Canvas ()

instance Stylable Picture where
    style (AlterBrush bmod) p = do
        (acts, oldbrush, eqs, i) <- get
        put (acts, bmod oldbrush, eqs, i)
        p
        (newacts, _, neweqs, newi) <- get
        put (newacts, oldbrush, neweqs, newi)
    style (Transform m) p = do
        modify $ \(acts, brush, eqs, i) ->
            ((\c -> save >> transform (m c)) : acts, brush, eqs, i)
        p
        modify $ \(acts, brush, eqs, i) ->
            (const restore : acts, brush, eqs, i)


newVar :: State Canvas (Variable t)
newVar = do
    (acts, bmod, eqs, i) <- get
    put (acts, bmod, eqs, i+1)
    return $ Var (i+1)

newEquation :: Equation Double -> State Canvas ()
newEquation eq = do
    (acts, bmod, eqs, i) <- get
    let neweqs = addEq eqs eq
    if rank eqs < rank neweqs then
        put (acts, bmod, neweqs, i)
    else
        error $ "Useless equation #" ++ show (size neweqs)


draw :: Picture -> [Render ()]
draw pic =
    let
        (acts, _, eqs, _) = execState pic emptyCanvas
        ctxt = solve eqs
    in
        if not (enoughEquations eqs) then error "Not enough equations!" else
        map (`obtain` ctxt) . reverse $ acts
