module Draw.Style.Colour where


type Colour = (Double, Double, Double, Double)

fromRGB :: Double -> Double -> Double -> Colour
fromRGB r g b = (r, g, b, 1.0)


black :: Colour
black = fromRGB 0.0 0.0 0.0

white :: Colour
white = fromRGB 1.0 1.0 1.0

red :: Colour
red = fromRGB 1.0 0.0 0.0

green :: Colour
green = fromRGB 0.0 1.0 0.0

blue :: Colour
blue = fromRGB 0.0 0.0 1.0

transparent :: Colour
transparent = (0.0, 0.0, 0.0, 0.0)
