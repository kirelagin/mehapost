{-# LANGUAGE FlexibleContexts #-}

module Draw.Style where


import Graphics.Rendering.Cairo hiding (Path)
import Graphics.Rendering.Cairo.Matrix as CM

import Draw
import Draw.Style.Colour
import Obtainable
import Variable


defaultBrush :: Brush
defaultBrush = Brush transparent black 1.0


data Alter = AlterBrush (Brush -> Brush)
           | Transform (Context Double -> Matrix)

fillColour :: Colour -> Alter
fillColour c = AlterBrush $ \b -> b {brushfc = c}

penColour :: Colour -> Alter
penColour c = AlterBrush $ \b -> b {brushsc = c}

penWidthMod :: (Double -> Double) -> Alter
penWidthMod f = AlterBrush $ \b -> b {brushsw = f (brushsw b)}

penWidth :: Double -> Alter
penWidth w = penWidthMod $ const w

moved :: (Obtainable Double (Context Double) s ,Obtainable Double (Context Double) t) =>
         (s, t) -> Alter
moved (dx, dy) = Transform $ \c -> CM.translate (obtain dx c) (-(obtain dy c)) CM.identity

scaledxy :: (Obtainable Double (Context Double) s, Obtainable Double (Context Double) t) =>
            (s, t) -> Alter
scaledxy (cx, cy) = Transform $ \c -> CM.scale (obtain cx c) (obtain cy c) CM.identity

scaled :: Obtainable Double (Context Double) t => t -> Alter
scaled c = scaledxy (c, c)

rotated :: Obtainable Double (Context Double) t => t -> Alter
rotated a = Transform $ \c -> CM.rotate (obtain a c * tau) CM.identity
    where tau = 2 * pi


(##) :: Stylable a => a -> Alter -> a
a ## smod = style smod a


class Stylable a where
    style :: Alter -> a -> a

instance Stylable b => Stylable (a -> b) where
    style smod f = style smod . f
