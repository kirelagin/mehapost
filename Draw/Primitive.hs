{-# LANGUAGE NoMonomorphismRestriction #-}

module Draw.Primitive where

import Control.Monad.Trans.State
import Graphics.Rendering.Cairo hiding (Path)

import Draw
import Draw.Canvas
import Variable


type Primitive = Context Double -> Path

paintPrimitive :: Primitive -> Brush -> Context Double -> PaintAction
paintPrimitive p brush c = paintPathWithBrush brush (p c)

primitivePic :: Primitive -> Picture
primitivePic p = modify $ \(acts, brush, eqs, i) ->
        (paintPrimitive p brush : acts, brush, eqs, i)


circlePath :: Path
circlePath = ClosedPath $ arc 0.0 0.0 1.0 0 (2*pi)

squarePath :: Path
squarePath = ClosedPath $ rectangle (-1/2) (- 1/2) 1.0 1.0

trianglePath :: Path
trianglePath = ClosedPath $ moveTo 0.0 (-t)
                         >> lineTo 0.5 (1/2 * t)
                         >> lineTo (-0.5) (1/2 * t)
                         >> closePath
    where t = 1 / sqrt 3

linePath :: Path
linePath = OpenPath $ moveTo (-0.5) 0.0 >> relLineTo 1.0 0.0


circle = primitivePic $ lift0 circlePath
square = primitivePic $ lift0 squarePath
triangle  = primitivePic $ lift0 trianglePath
line = primitivePic $ lift0 linePath
