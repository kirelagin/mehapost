module Util where


uncurry4 :: (a -> b -> c -> d -> z) -> (a, b, c, d) -> z
uncurry4 f (a, b, c, d) = f a b c d
