{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, UndecidableInstances #-}

module Variable where


import Data.Maybe

import Obtainable


newtype Known a = Known a deriving (Eq, Show)
instance Obtainable t c (Known t) where
    obtain (Known v) _ = v

newtype Variable t = Var Integer deriving (Eq, Ord)

instance Show (Variable t) where
    show (Var s) = "var" ++ show s

type Context a = [(Variable a, a)]

instance Obtainable t (Context t) (Variable t) where
    obtain v = fromJust . lookup v


lift0 :: s
      -> c -> s
lift0 s _ = s

lift1 :: Obtainable d1 c x1
      => (d1 -> s)
      -> x1
      -> c -> s
lift1 s v1 c = s (obtain v1 c)

lift2 :: (Obtainable d1 c x1, Obtainable d2 c x2)
      => (d1 -> d2 -> s)
      -> x1 -> x2
      -> c -> s
lift2 s v1 v2 c = s (obtain v1 c) (obtain v2 c)
