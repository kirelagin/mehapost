module Output where

import Graphics.Rendering.Cairo
import Draw.Canvas


drawPNG :: String -> Int -> Int -> Picture -> IO ()
drawPNG path width height pic =
    let
        prepare = translate (fromIntegral width/2) (fromIntegral height/2)
        image   = (sequence_ . draw) pic :: Render ()
        finish  = return ()
    in
        withImageSurface FormatARGB32 width height $ \surface ->
            renderWith surface (prepare>>image>>finish) >>
            surfaceWriteToPNG surface path
