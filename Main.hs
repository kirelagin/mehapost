import Obtainable
import Variable
import Draw.Style
import Draw.Primitive
import Draw.Style.Colour
import Equations
import Draw.Canvas
import Output


val :: Fractional t => Variable t -> Context t -> t
val = obtain

c :: Double -> Double
c = id

redcircle :: Picture
redcircle = circle ## fillColour red
                   ## scaled (c 20)

house :: Picture
house = do
    totalHeight <- newVar
    wallWidth <- newVar
    wallHeight <- newVar
    roofWidth <- newVar
    roofHeight <- newVar
    roofAbove <- newVar
    windowR <- newVar

    newEquation $ totalHeight #=# wallHeight #+# roofHeight
    newEquation $ wallHeight #=# wallWidth -- square wall
    newEquation $ roofWidth #=# wallWidth #*. 1.3 -- roof is a bit wider
    newEquation $ wallHeight #=# roofHeight #*. 1.0
    newEquation $ totalHeight #=. 100
    newEquation $ roofAbove #=# wallHeight #*. 0.45 #+# roofHeight #*. (0.5 / sqrt 3)
    newEquation $ windowR #=# roofWidth #/. 10

    let top = do
        triangle ## scaledxy (roofWidth, roofHeight) -- roof
                 ## fillColour green
        circle ## scaled windowR -- window in the roof
               ## fillColour blue

    let bottom = do -- assume ground is at y=0
        let doorHeight = val wallHeight * 1/2
            doorWidth =  val wallWidth / 4
        square ## scaledxy (wallWidth, wallHeight) -- wall
               ## fillColour white
               ## moved (c 0, val wallHeight / 2)
        square ## scaledxy (doorWidth, doorHeight) -- door
               ## moved (val wallWidth / 2 - doorWidth / 2 - 5, doorHeight / 2)
               ## fillColour red

    bottom ## moved (c 0, -val wallHeight / 2)
    top ## moved (c 0, roofAbove)


mypic :: Picture
mypic = house ## penColour black
              ## scaled (c 10)
              ## penWidth 15

main :: IO ()
main = drawPNG path width height mypic
  where
    width = 2000
    height = 2000
    path = "/home/kirrun/tmp/res.png"
